# minireco.

_minireco_ is a simple [recommender system](https://en.wikipedia.org/wiki/Recommender_system#Hybrid_recommender_systems)
test, written in Python 3, that uses sqlite as a backend. Still no more than a
mere experiment.
