import collections

SongItem = collections.namedtuple('SongItem', ('name', 'speed', 'decade'))


items = [
    SongItem('Aftermath',      'slow',   '1980'),
    SongItem('Antevision',     'medium', '2000'),
    SongItem('Apricot',        'slow',   '2000'),
    SongItem('Barium',         'fast',   '1990'),
    SongItem('Barn',           'slow',   '1980'),
    SongItem('Barney',         'medium', '1990'),
    SongItem('Burns',          'fast',   '2010'),
    SongItem('Cockstop',       'medium', '2010'),
    SongItem('Critter',        'slow',   '1980'),
    SongItem('Dipperman',      'slow',   '2000'),
    SongItem('Duped',          'medium', '1990'),
    SongItem('Elastic Mayhem', 'fast',   '2000'),
    SongItem('Enol',           'slow',   '1990')
]

item_names = [x.name for x in items]

LineApprox = collections.namedtuple('LineApprox', ('name', 'position'))

speeds = [
    LineApprox('fast',   2),
    LineApprox('medium', 1),
    LineApprox('slow',   0)
]

decades = [
    LineApprox('1980', -3),
    LineApprox('1990', -2),
    LineApprox('2000', -1),
    LineApprox('2010',  0),
]

def find_item(name):
    if name not in item_names:
        raise BaseException(f'No such item \'{name}\' found!')

    return [x for x in items if x.name == name][0]

def find_speed(item):
    candidates = [x for x in speeds if x.name == item]

    if len(candidates) == 0:
        raise BaseException(f"Speed '{item}' not found!")

    else:
        return candidates[0]

def find_decade(item):
    candidates = [x for x in decades if x.name == item]

    if len(candidates) == 0:
        raise BaseException(f"Decade '{item}' not found!")

    else:
        return candidates[0]

def distance_generic(item1, item2):
    return abs(item1.position - item2.position)

def speed_distance(item1, item2):
    return abs(find_speed(item1).position - find_speed(item2).position)

def decade_distance(item1, item2):
    return abs(find_decade(item1).position - find_decade(item2).position)

def item_distance(item1, item2):
    return (speed_distance(item1.speed, item2.speed) ** 2 + decade_distance(item1.decade, item2.decade) ** 2) ** 0.5